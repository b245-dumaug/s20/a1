let number = prompt('Please enter a number:');
console.log('The number you provided is ' + number);
for (let i = number; i >= 0; i--) {
  if (i <= 50) {
    break;
  } else if (i % 10 === 0) {
    console.log('The number is divisible by 10. Skipping the number.');
    continue;
  } else if (i % 5 === 0) {
    console.log(i);
  }
}

let word = 'supercalifragilisticexpialidocious';
console.log(word);
let consonants = '';
for (let i = 0; i < word.length; i++) {
  if (
    word[i] === 'a' ||
    word[i] === 'e' ||
    word[i] === 'i' ||
    word[i] === 'o' ||
    word[i] === 'u'
  ) {
    continue;
  } else {
    consonants += word[i];
  }
}
console.log(consonants);
